<?php
namespace src\Bitm\SEIP136053\Book;
class Book{
    public $id ="";
    public $title ="";
    public $created ="";
    public $modified ="";
    public $created_by ="";
    public $modified_by ="";

    public function index()
    {
    return "I am listing data";
    }
    public function view()
    {
        return "I am viewing data";
    }
    public function create()
    {
        return "I am create form";
    }
    
    public function store()
    {
        return "I am storing data";
    }
    public function edit()
    {
        return "I am editing form";
    }
    public function update()
    {
        return "I am updating data";
    }
    public function delete()
    {
        return "I am deleting data";
    }

}
?>